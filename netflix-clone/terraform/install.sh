#!/bin/bash

# Update the system
sudo yum update -y

# Install Docker
sudo amazon-linux-extras install docker
sudo service docker start
sudo usermod -a -G docker ec2-user

# Install Trivy
sudo curl -L "https://github.com/aquasecurity/trivy/releases/download/v0.21.0/trivy_0.21.0_Linux-64bit.tar.gz" -o trivy_0.21.0_Linux-64bit.tar.gz
sudo tar zxvf trivy_0.21.0_Linux-64bit.tar.gz -C /usr/local/bin

# Install Java 11 (required for SonarQube)
sudo yum install java-11-openjdk-devel -y

# Install Jenkins
sudo wget https://pkg.jenkins.io/redhat-stable/jenkins.repo -O /etc/yum.repos.d/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
sudo yum install jenkins -y
sudo systemctl start jenkins
sudo systemctl enable jenkins

# Install SonarQube
sudo wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-8.9.6.50800.zip -O sonarqube.zip
sudo yum install unzip -y
sudo unzip sonarqube.zip -d /opt
sudo mv /opt/sonarqube-8.9.6.50800 /opt/sonarqube
sudo chown -R ec2-user:ec2-user /opt/sonarqube
sudo /opt/sonarqube/bin/linux-x86-64/sonar.sh start
