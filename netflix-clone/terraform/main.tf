provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "netflix-clone" {
  ami           = "ami-0c55b159cbfafe1f0"  # replace with the AMI ID for your region
  instance_type = "t2.large"

  user_data = <<-EOF
              #!/bin/bash
              cat <<'SCRIPT' > install.sh
              ${file("install.sh")}
              SCRIPT
              chmod +x install.sh
              ./install.sh
              EOF

  tags = {
    Name = "t2-large-jenkins-scanning"
  }
}
